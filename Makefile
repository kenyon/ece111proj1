# vlib work
# creates the work directory

all: fibonacci_calculator tb_fibonacci_calculator

fibonacci_calculator:
	vlog fibonacci_calculator.v

tb_fibonacci_calculator:
	vlog tb_fibonacci_calculator.v
